module.exports = {
  plugins: [{
  	resolve: `gatsby-plugin-typography`,
  	options: {
  		pathToConfigModule: `src/utils/typography.js`,
  	},
  }],
  pathPrefix: '/gatsby-2-css-modules'
}
